from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LogInForm

def signup(request):
    form = SignUpForm()
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    first_name=first_name,
                    last_name=last_name,
                    password=password)
                login(request, user)
                return redirect("recipe_list")
            else:
                form.add_error("password", "passwords do not match")
        else:
            form = SignUpForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)

def user_login(request):
  form = LogInForm()
  if request.method == "POST":
    # Get the form data.
    form = LogInForm(request.POST)

    # Validate the form data.
    if form.is_valid():
      # Get the username and password from the form data.
      username = form.cleaned_data["username"]
      password = form.cleaned_data["password"]

      # Authenticate the user.
      user = authenticate(request, username=username, password=password)

      # If the user is authenticated, log them in and redirect to the recipe list page.
      if user is not None:
        login(request, user)
        return redirect("recipe_list")

    # If the form data is not valid, show the login form again.
    else:
      form = LogInForm()

  # Get the context for the template.
  context = {
    "form": form,
  }

  # Render the login template.
  return render(request, "accounts/login.html", context)

def user_logout(request):
    logout(request)
    return redirect("recipe_list")
